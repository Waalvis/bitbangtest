import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:prueba_tienda_deportiva_flutter/screens/home_page.dart';
import 'package:prueba_tienda_deportiva_flutter/utilies/ProductDataSource.dart';
import 'package:prueba_tienda_deportiva_flutter/utilies/product_model.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

openPopup(context) {
  final TextEditingController _controllerName = TextEditingController();
  final TextEditingController _controllerDescription = TextEditingController();
  final TextEditingController _controllerCount = TextEditingController();
  ProductDataSource productDataSource = ProductDataSource();

  Alert(
      context: context,
      title: "Create Product",
      content: Column(
        children: <Widget>[
          TextField(
            controller: _controllerName,
            decoration: InputDecoration(
              hintText: 'Enter Name',
              icon: Icon(Icons.looks_one_outlined),
            ),
          ),
          TextField(
            controller: _controllerDescription,
            decoration: InputDecoration(
              hintText: 'Enter Description',
              icon: Icon(Icons.looks_two_outlined),
            ),
          ),
          TextField(
            keyboardType: TextInputType.number,
            controller: _controllerCount,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            decoration: InputDecoration(
              hintText: 'Enter Quantity',
              icon: Icon(Icons.looks_two_outlined),
            ),
          )
        ],
      ),
      buttons: [
        DialogButton(
          onPressed: () {
            ProductModel newProduct = ProductModel(
                name: _controllerName.text,
                description: _controllerDescription.text,
                countStock: int.parse(_controllerCount.text),
                image:
                    'https://c8.alamy.com/compes/k4ky63/carrito-de-compras-con-botellas-beba-productos-icono-k4ky63.jpg');
            productDataSource.addNewProduct(newProduct);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return HomePage();
                },
              ),
            );
          },
          child: Text(
            "SEND",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        )
      ]).show();
}
